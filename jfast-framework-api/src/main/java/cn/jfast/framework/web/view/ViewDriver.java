/*
 * Copyright 2015 泛泛o0之辈
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.web.view;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;

import cn.jfast.framework.base.util.Assert;
import cn.jfast.framework.log.LogFactory;
import cn.jfast.framework.log.LogType;
import cn.jfast.framework.log.Logger;
import cn.jfast.framework.base.prop.CoreConsts;
import cn.jfast.framework.web.api.Api;
import cn.jfast.framework.web.api.HttpRequest;
import cn.jfast.framework.web.api.ApiInvocation;
import cn.jfast.framework.web.api.ApiContext;
import cn.jfast.framework.web.api.HttpResponse;
import cn.jfast.framework.web.view.viewtype.*;

/**
 * 视图驱动
 */
public class ViewDriver {

    private Logger log = LogFactory.getLogger(LogType.JFast, ViewDriver.class);

    private List<Exception> expList = new ArrayList<Exception>();

    private final String TEXT_HTML = "text/html;charset=";

    private final String APPLICATION_DOWNLOAD = "application/x-msdownload";

    private final String APPLICATION_JSON = "application/json; charset=";

    private final String CONTENT_DISPOSITION = "Content-Disposition";

    private final String CONTENT_LENGTH = "Content-Length";
    
    private boolean isRendered = false;

    public synchronized void render(Object view, HttpRequest request,
    		HttpResponse response, List<Exception> ex) {
    	if(isRendered)  // 
    		return;
        if (null == ex || ex.isEmpty()) {
            if(null != view) {
                if (view instanceof Jsp)
                    this.renderJSP((Jsp) view, request, response);
                else if (view instanceof Text)
                    this.renderText((Text) view, request, response);
                else if (view instanceof Html)
                    this.renderHtml((Html) view, request, response);
                else if (view instanceof Image)
                    this.renderImage((Image) view, request, response);
                else if (view instanceof Json)
                    this.renderJSON((Json) view, request, response);
                else if (view instanceof Download)
                    this.renderDownload((Download) view, request, response);
                else if (view instanceof cn.jfast.framework.web.view.viewtype.Api)
                    this.renderService((cn.jfast.framework.web.view.viewtype.Api) view, request, response);
                else if (view instanceof String)
                    this.renderJSP(new Jsp((String) view), request, response);
                else
                    this.renderNull();
            }
        } else {
            try {
                for(Exception e:ex){
                    log.error("",e);
                }
                if (CoreConsts.develop_mode) {
                    String expHtml = "";
                    response.reset();
                    response.setContentType(TEXT_HTML + CoreConsts.encoding);
                    for(Exception e:ex){
                        expHtml += generateException(e);
                    }
                    response.getWriter().println(expHtml);
                }
            } catch (IOException e) {
                log.error("",e);
            }

        }
        isRendered = true;
    }

    private void renderImage(Image view, HttpRequest request,
			HttpResponse response) {
    	try {
    		response.setContentType("application/binary;charset=ISO8859_1");
            OutputStream outs = response.getOutputStream();
            outs.write(view.getView());
            outs.flush();
        }catch (IOException e) {
            log.error("",e);
        }
	}

	private void renderHtml(Html view, HttpRequest request, HttpResponse response) {
        try {
        	if(view.getView().startsWith("http://") || view.getView().startsWith("https://")){
        		response.sendRedirect(view.getView());
        	} else {
	        	String path = request.getContextPath();
	    		String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";	
	            response.sendRedirect(basePath+view.getView());
        	}
        }catch (IOException e) {
            log.error("",e);
        }
    }

    private void renderText(Text view, HttpRequest request, HttpResponse response) {
        try {
            response.setCharacterEncoding(CoreConsts.encoding);
            response.setContentType(view.getContentType());
            response.getWriter().println(view.getView());
        } catch (IOException e) {
            log.error("",e);
        } finally {
            try {
                response.getWriter().close();
            } catch (IOException e) {
                log.error("",e);
            }
        }
    }

    private void renderService(cn.jfast.framework.web.view.viewtype.Api view, HttpRequest request,
    		HttpResponse response) {
        for (Entry<String, String> requestAttr : view.getActionAttrs().entrySet()) {
            request.setAttribute(requestAttr.getKey(), requestAttr.getValue());
        }
        Api action = ApiContext.getApi(view.getView() + "/$" + request.getMethod().toLowerCase() + "$");
        Assert.notNull(action, "找不到对应API [" + view.getView() + "/$" + request.getMethod().toLowerCase() + "$]");
        try {
            new ApiInvocation(action, action.getApi().newInstance(), (HttpRequest)request, response, view.getView() + "/$" + request.getMethod().toLowerCase() + "$").invoke();
        } catch (InstantiationException e) {
            expList.add(e);
            this.render(view,request,response,expList);
        } catch (IllegalAccessException e) {
            expList.add(e);
            this.render(view,request,response,expList);
        }
    }

    private void renderDownload(Download view, HttpRequest request,
                                HttpResponse response) {
        ServletOutputStream out = null;
        try {
            response.setCharacterEncoding(CoreConsts.encoding);
            String fileName = response.encodeURL(new String(view.getFilename().getBytes(), CoreConsts.encoding));
            if (view.getMode() == DownloadMode.OPTION) {
                response.setContentType(APPLICATION_DOWNLOAD);
                response.setHeader(CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"");
            } else {
                response.setContentType(view.getContentType());
                response.setHeader(CONTENT_DISPOSITION, "inline; filename=\"" + fileName + "\"");
            }
            response.addHeader(CONTENT_LENGTH, "" + view.getFileData().length);
            response.setContentType(view.getContentType());
            out = response.getOutputStream();
            out.write(view.getFileData());
            response.setStatus(HttpResponse.SC_OK);
            response.flushBuffer();
        } catch (UnsupportedEncodingException e) {
            log.error("", e);
        } catch (IOException e) {
            log.error("", e);
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                log.error(e.getMessage());
            }
        }
    }

    private void renderJSP(Jsp view, HttpRequest request,
                           HttpResponse response) {

        try {
            if (view.getRoute() == Route.REDIRECT) {
                response.setCharacterEncoding(CoreConsts.encoding);
                response.setContentType(TEXT_HTML + CoreConsts.encoding);
                response.sendRedirect(request.getContextPath()
                        + "/" + view.getView());
            } else {
                for (Entry<String, Object> requestAttr : view.getRequestAttrs().entrySet()) {
                    request.setAttribute(requestAttr.getKey(), requestAttr.getValue());
                }
                request.getRequestDispatcher(
                        "/" + view.getView()).forward(
                        request, response);
            }
        } catch (ServletException e) {
            log.error("", e);
        } catch (IOException e) {
            log.error("", e);
        }

    }

    private void renderJSON(Json view, HttpRequest request,
                            HttpResponse response) {
        try {
            response.setCharacterEncoding(CoreConsts.encoding);
            response.setContentType(APPLICATION_JSON + CoreConsts.encoding);
            response.getWriter().println(view.getView());
        } catch (IOException e) {
            log.error("", e);
        } finally {
            try {
                response.getWriter().close();
            } catch (IOException e) {
                log.error("",e);
            }
        }

    }
    
    private void renderNull() {}
    
    private String generateException(Exception ex) {
        Throwable t = ex.getCause();
        StringBuffer sbf = new StringBuffer("<p style='font-family:Micro Yahei;font-size:12px;padding-left:20px;color:red;'>");
        sbf.append(ex.toString() + "<br>");
        StackTraceElement[] exs = ex.getStackTrace();
        for (StackTraceElement es : exs) {
            sbf.append("at " + es.getClassName() + "." + es.getMethodName() + "(" + es.getFileName() + ":" + es.getLineNumber() + ")<br>");
        }
        if (null != t) {
            sbf.append("<b>Cause By</b> : " + t.toString() + "<br>");
            StackTraceElement[] txs = t.getStackTrace();
            for (StackTraceElement es : txs) {
                sbf.append("at " + es.getClassName() + "." + es.getMethodName() + "(" + es.getFileName() + ":" + es.getLineNumber() + ")<br>");
            }
            sbf.append("</p>");
        }
        return sbf.toString();
    }
}
