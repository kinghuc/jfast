package cn.jfast.framework.jdbc.orm;

import cn.jfast.framework.base.util.ErrorCode;

/**
 * Created by jfast on 2015/6/11.
 */
public class SqlException extends Exception{

	private static final long serialVersionUID = 1L;

	public SqlException(ErrorCode code){
        super(code.getErrorMsg());
    }

}
