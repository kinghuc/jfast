package cn.jfast.expand.wx.api.consts;

/**
 * 微信所有的API地址
 */
public interface WxApiUrl {
    /**
     * 令牌API入口
     */
    String ACCESS_TOKEN_API = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%1$s&secret=%2$s";
    /**
     * 获取微信服务器IP
     */
    String IP_LIST_API = "https://api.weixin.qq.com/cgi-bin/getcallbackip?access_token=%1$s";
    /**
     * 自定义菜单API入口[create, get, delete]
     */
    String CUSTOM_MENU_API = "https://api.weixin.qq.com/cgi-bin/menu/%1$s?access_token=%2$s";
    /**
     * 分组管理API入口[create, get, getid, update]
     */
    String GROUPS_API = "https://api.weixin.qq.com/cgi-bin/groups/%1$s?access_token=%2$s";
    /**
     * 用户分组API入口
     */
    String GROUPS_USER_API = "https://api.weixin.qq.com/cgi-bin/groups/members/update?access_token=%1$s";
    /**
     * 微信用户信息API入口
     */
    String USER_INFO_API = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=%1$s&openid=%2$s&lang=zh_CN";
    /**
     * 公众号关注者API入口
     */
    String FOLLOWS_USER_API = "https://api.weixin.qq.com/cgi-bin/user/get?access_token=%1$s&next_openid=%2$s";
    /**
     * 下载多媒体文件API入口
     */
    String MEDIA_DL_API = "http://file.api.weixin.qq.com/cgi-bin/media/get?access_token=%1$s&media_id=%2$s";
    /**
     * 上传多媒体文件API入口
     */
    String MEDIA_UP_API = "http://file.api.weixin.qq.com/cgi-bin/media/upload?type=%1$s&access_token=%2$s";
    /**
     * 发送客服消息入口
     */
    String CUSTOM_MESSAGE_API = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=%1$s";
    /**
     * 网页授权请求地址
     */
    String WEB_OAUTH2 = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=%1$s&redirect_uri=%2$s&response_type=code&scope=%3$s&state=%4$s#wechat_redirect";
    /**
     * 网页授权获取TOKEN
     */
    String OAUTH_TOKEN = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=%1$s&secret=%2$s&grant_type=authorization_code&code=%3$s";
    /**
     * 网页授权取得用户信息地址
     */
    String OAUTH_USERINFO = "https://api.weixin.qq.com/sns/userinfo?access_token=%1$s&openid=%2$s&lang=%3$s";
    /**
     * 发送模板消息地址
     */
    String TEMPLATE_MESSAGE_API = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=%1$s";
    /**
     * 上传图文素材地址
     */
    String NEWS_UPLOAD_API = "https://api.weixin.qq.com/cgi-bin/media/uploadnews?access_token=%1$s";
    /**
     * 分组群消息[sendall,send,delete]入口
     */
    String GROUP_NEWS_MESSAGE_API = "https://api.weixin.qq.com/cgi-bin/message/mass/%1$s?access_token=%2$s";
    /**
     * 群发消息上传视频地址
     */
    String MEDIA_UPVIDEO_API = "http://file.api.weixin.qq.com/cgi-bin/media/uploadvideo?access_token=%1$s";
    /**
     * 服务组件API入口
     */
    String COMPONENT_API = "https://api.weixin.qq.com/cgi-bin/component/%1$s?component_access_token=%2$s";
    /**
     * 获取服务组件token地址
     */
    String COMPONENT_TOKEN_API = "https://api.weixin.qq.com/cgi-bin/component/api_component_token";
    /**
     * 获取JSTICKET地址
     */
    String JSAPI_TICKET_URL = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=jsapi&access_token=";
}
