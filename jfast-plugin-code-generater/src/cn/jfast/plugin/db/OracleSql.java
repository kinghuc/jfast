/**
 * 
 */
package cn.jfast.plugin.db;

/**
 * MySql数据对象
 * 
 * @author jfast 2015年8月18日
 */
public class OracleSql extends DbBase{

	private static OracleSql instance = null;

	static {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

	}

	public static OracleSql instance() {
		if (instance == null) {
			instance = new OracleSql();
		}
		return instance;
	}

	private OracleSql() {
		super();
	}

}
