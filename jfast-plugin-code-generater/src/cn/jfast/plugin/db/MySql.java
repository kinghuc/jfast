/**
 * 
 */
package cn.jfast.plugin.db;

/**
 * MySql数据对象
 * 
 * @author jfast 2015年8月18日
 */
public class MySql extends DbBase{

	private static MySql instance = null;

	static {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

	}

	public static MySql instance() {
		if (instance == null) {
			instance = new MySql();
		}
		return instance;
	}

	private MySql() {
		super();
	}

}
